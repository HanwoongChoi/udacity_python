# Quiz: Top Three
# Write a function, top_three, that takes a list as its argument, and returns a list of the three largest elements. For example, top_three([2,3,5,6,8,4,2,1]) == [8, 6, 5]
def top_three(input_list):
    """Returns a list of the three largest elements input_list in order from largest to smallest.

    If input_list has fewer than three elements, return input_list element sorted largest to smallest/
    """
    # TODO: implement this function
    return sorted(input_list, reverse=True)[:3]


print(top_three([2, 3, 5, 6, 8, 4, 2, 1]))


# Quiz: Median
# The function in this quiz, median, returns the median value of an input list. Unfortunately it only works with lists that have an odd number of elements. Modify the function so that when median is given a list with an even number of elements, it returns the mean of the two central elements. The provided test cases demonstrate the expected behavior.
def median(numbers):
    numbers.sort()  # The sort method sorts a list directly, rather than returning a new sorted list
    middle_index = int(len(numbers) / 2)
    if len(numbers) % 2 == 0:
        return float(numbers[middle_index - 1] + numbers[middle_index]) / 2
    else:
        return numbers[middle_index]


test1 = median([1, 2, 3])
print("expected result: 2, actual result: {}".format(test1))

test2 = median([1, 2, 3, 4])
print("expected result: 2.5, actual result: {}".format(test2))

test3 = median([53, 12, 65, 7, 420, 317, 88])
print("expected result: 65, actual result: {}".format(test3))


# Quiz: Sum of a List
# Define a function, list_sum, that takes a list as its argument and returns the sum of the elements in the list. Use a for loop to iterate over the list
def list_sum(input_list):
    sum = 0
    # todo: Write a for loop that adds the elements
    # of input_list to the sum variable
    for input in input_list:
        sum += input
    return sum


# These test cases check the list_sum works correctly
test1 = list_sum([1, 2, 3])
print("expected result: 6, actual result: {}".format(test1))

test2 = list_sum([-1, 0, 1])
print("expected result: 0, actual result: {}".format(test2))

# Quiz: XML Tag Counter
# Write a function, tag_count, that takes as its argument a list of strings. It should return a count of how many of those strings are XML tags. XML is a data language similar to HTML. You can tell if a string is an XML tag if it begins with a left angle bracket "<" and end with a right angle bracket ">".
#
# You can assume that the list of string that will be given as input will not contain empty strings.
"""Write a function, `tag_count`, that takes as its argument a list
of strings. It should return a count of how many of those strings
are XML tags. You can tell if a string is an XML tag if it begins
with a left angle bracket "<" and end with a right angle bracket ">".
"""


# TODO: Define the tag_count function
def tag_count(list):
    sum = 0
    for item in list:
        if (item[0] == '<') and (item[-1] == '>'):
            sum += 1
    return sum


# Test for the tag_count function:
list1 = ['<greeting>', 'Hello World!', '</greeting>']
count = tag_count(list1)
print("Expected result: 2, Actual result: {}".format(count))


# Quiz: Create an HTML List
# Write the html_list function. The function takes one argument, a list of strings, and returns a single string which is an HTML list. For example, if the function should produce the following string when provided the list ['first string', 'second string'].
#
# <ul>
# <li>first string</li>
# <li>second string</li>
# </ul>
# That is, the string's first line should be the opening tag <ul>. Following that is one line per element in the source list, surrounded by <li> and </li> tags. The final line of the string should be the closing tag </ul>.
# define the  html_list function
def html_list(tokens):
    ret = "<ul>\n"
    for token in tokens:
        ret += "<li>{}</li>\n".format(token)
    ret += "</ul>\n"
    return ret


print(html_list(['first string', 'second string']))


# Quiz: Starbox
# The starbox function in the quiz below prints a box made out of asterisks. The function takes two arguments, width and height, that specify how many characters wide the box is and how many lines tall it is. The function isn't quite complete: it prints boxes of the correct width, but the height argument is ignored. Complete the function so that both of the provided test cases print boxes that are the correct size. Hint: The range function could be helpful!
def starbox(width, height):
    """print a box made up of asterisks.

    width: width of box in characters, must be at least 2
    height: height of box in lines, must be at least 2
    """
    print("*" * width)  # print top edge of box

    # print sides of box
    # todo: print this line height-2 times, instead of three times
    for i in range(height - 2):
        print("*" + " " * (width - 2) + "*")

    print("*" * width)  # print bottom edge of box


# Test Cases
print("Test 1:")
starbox(5, 5)  # this prints correctly

print("Test 2:")
starbox(2, 3)  # this currently prints two lines too tall - fix it!


# Quiz: Nearest Square
# Implement the nearest_square function. The function takes an integer argument limit, and returns the largest square number that is less than limit. A square number is the product of an integer multiplied by itself, for example 36 is a square number because it equals 6*6.
#
# There's more than one way to write this code, but I suggest you use a while loop!
#
# Here is a test case you can copy to test your code. Feel free to write additional tests too!
#
# test1 = nearest_square(40)
# print("expected result: 36, actual result: {}".format(test1))
# TODO: Implement the nearest_square function
def nearest_square(num):
    ret = 0
    while (ret + 1) ** 2 <= num:
        ret += 1
    return ret ** 2

    # def nearest_square(limit):
    answer = 0
    while (answer + 1) ** 2 < limit:
        answer += 1
    return answer ** 2


test1 = nearest_square(40)
print("expected result: 36, actual result: {}".format(test1))

# Quiz: Break the String
# Time to write your own loop with a break statement. Your task is to create a string, news_ticker that is exactly 140 characters long. You should create the news ticker by adding headlines from the headlines list, inserting a space in between each headline. If necessary, truncate the last headline in the middle so that news_ticker is exactly 140 characters long.
#
# Remember that break works in both for and while loops. Use whichever loop seems most appropriate. Consider adding print statements to your code to help you resolve bugs.
headlines = ["Local Bear Eaten by Man",
             "Legislature Announces New Laws",
             "Peasant Discovers Violence Inherent in System",
             "Cat Rescues Fireman Stuck in Tree",
             "Brave Knight Runs Away",
             "Papperbok Review: Totally Triffic"]

news_ticker = ""
# TODO: set news_ticker to a string that contains no more than 140 characters long.
# HINT: modify the headlines list to verify your loop works with different inputs
for headline in headlines:
    news_ticker += headline + " "
    if len(news_ticker) >= 140:
        news_ticker = news_ticker[:140]
        break

print(news_ticker)

# Quiz: Build a Set
# In a similar way to building an empty list with my_list = [], you can create an empty set with my_set = set(). Using this technique, and the add method, build a set of all of the square numbers greater than 0 and less than 2,000. For reference, I included my implementation of nearest_square function from an earlier quiz. You may call the function in your code, integrate it into your code, or ignore it altogether.
squares = set()


# todo: populate "squares" with the set of all of the integers less
# than 2000 that are square numbers


# Note: If you want to call the nearest_square function, you must define
# the function on a line before you call it. Feel free to move this code up!
def nearest_square(limit):
    answer = 0
    while (answer + 1) ** 2 < limit:
        answer += 1
    return answer ** 2


num = 2000
while nearest_square(num) > 0:
    num = nearest_square(num)
    squares.add(num)

print(squares)

# Quiz: Prolific Year
# Write a function most_prolific that takes a dict formatted like Beatles_Discography example above and returns the year in which the most albums were released. If you call the function on the Beatles_Discography it should return 1964, which saw more releases than any other year in the discography.
#
# If there are multiple years with the same maximum number of releases, the function should return a list of years.
Beatles_Discography = {"Please Please Me": 1963, "With the Beatles": 1963,
                       "A Hard Day's Night": 1964, "Beatles for Sale": 1964, "Twist and Shout": 1964,
                       "Help": 1965, "Rubber Soul": 1965, "Revolver": 1966,
                       "Sgt. Pepper's Lonely Hearts Club Band": 1967,
                       "Magical Mystery Tour": 1967, "The Beatles": 1968,
                       "Yellow Submarine": 1969, 'Abbey Road': 1969,
                       "Let It Be": 1970}


def most_prolific(dic):
    years = {}
    for album_title in dic:
        year = dic[album_title]
        if year in years:
            years[year] += 1
        else:
            years[year] = 1
    return max(years, key=years.get)


# Quiz: Adding Values to Nested Dictionaries
# Try your hand at working with nested dictionaries. Add another entry, 'is_noble_gas' to each dictionary in the elements dictionary. After inserting the new entries you should be able to perform these lookups:
#
# >>> print(elements['hydrogen']['is_noble_gas'])
# False
# >>> print(elements['helium']['is_noble_gas'])
# True
elements = {'hydrogen': {'number': 1, 'weight': 1.00794, 'symbol': 'H'},
            'helium': {'number': 2, 'weight': 4.002602, 'symbol': 'He'}}

# todo: Add an 'is_noble_gas' entry to the hydrogen and helium dictionaries
# hint: helium is a noble gas, hydrogen isn't디드
elements['hydrogen']['is_noble_gas'] = False
elements['helium']['is_noble_gas'] = True

# Quiz: Flying Circus Records
# A regular flying circus happens twice or three times a month. For each month, information about the amount of money taken at each event is saved in a list, so that the amounts appear in the order in which they happened. The months' data is all collected in a dictionary called monthly_takings.
#
# For this quiz, write a function total_takingsthat calculates the sum of takings from every circus in the year.
monthly_takings = {'January': [54, 63], 'February': [64, 60], 'March': [63, 49],
                   'April': [57, 42], 'May': [55, 37], 'June': [34, 32],
                   'July': [69, 41, 32], 'August': [40, 61, 40], 'September': [51, 62],
                   'October': [34, 58, 45], 'November': [67, 44], 'December': [41, 58]}


def total_takings(yearly_record):
    pass  # TODO: Implemenent this function
    ret = 0
    for record in yearly_record:
        sum_list = yearly_record[record]
        for item in sum_list:
            ret += item
    return ret


def total_takings(yearly_record):
    # total is used to sum up the monthly takings
    total = 0
    for month in monthly_takings.keys():
        # I use the Python function sum to sum up over
        # all the elements in a list
        total = total + sum(monthly_takings[month])
    return total


print(total_takings(monthly_takings))


