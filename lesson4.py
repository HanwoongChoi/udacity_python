# Quiz: Days and Hours
# Try your hand at writing a function that uses a tuple to return multiple values. Write an hours2days function that takes one argument, an integer, that is a time period in hours. The function should return a tuple of how long that period is in days and hours, with hours being the remainder that can't be expressed in days. For example, 39 hours is 1 day and 15 hours, so the function should return (1,15).
#
# These examples demonstrate how the function can be used:
#
# >>> hours2days(24) # 24 hours is one day and zero hours
# (1, 0)
# >>> hours2days(25) # 25 hours is one day and one hour
# (1, 1)
# >>> hours2days(10000)
# (416, 16)
def hours2days(input_hours):
    days = input_hours // 24
    hours = input_hours % 24
    return days, hours


print(hours2days(10000))


# Quiz: Default Arguments
# print_list is a function that takes a list as its input and prints it line by line as a numbered or bulleted list. It takes three arguments:
#
# l: The list to print
# numbered: set to True to print a numbered list.
# bullet_character: The symbol placed before each list element. This is ignored if numbered is True.
# This function is pretty cumbersome to call. It requires a bullet_character even if the user wants a numbered list!
#
# Make the function easier to use by adding default arguments. By default the function should produce a bulleted list, and the default bullet character should be "-".
#
# After your changes, the function should behave like this:
#
# >>> print_list(["cats", "in", "space"])
# - cats
# - in
# - space
# >>> print_list(["cats", "in", "space"], True)
# 1: cats
# 2: in
# 3: space
def print_list(l, numbered=False, bullet_character='-'):
    """Prints a list on multiple lines, with numbers or bullets

    Arguments:
    l: The list to print
    numbered: set to True to print a numbered list
    bullet_character: The symbol placed before each list element. This is
                      ignored if numbered is True.
    """
    for index, element in enumerate(l):
        if numbered:
            print("{}: {}".format(index + 1, element))
        else:
            print("{} {}".format(bullet_character, element))


print_list(["cats", "in", "space"])
print_list(["cats", "in", "space"], True)


def create_cast_list(filename):
    cast_list = []
    # use with to open the file filename
    # use the for loop syntax to process each line
    # and add the actor name to cast_list

    with open(filename, 'r') as f:
        for line in f:
            cast_list.append(line.strip().split(',')[0])
    return cast_list


print(create_cast_list("flying_circus_cast.txt"))

# Quiz: Find the Greatest Common Divisor
# It's your turn to import and use the math module. Use the math module to calculate e to the power of 3. print the answer.
#
# Refer to the math module's documentation to find the function you need! https://docs.python.org/3.6/library/math.html?highlight=math%20module#module-math
# TODO: print e to the power of 3 using the math module
import math

print(math.exp(3))

# Quiz: Password Generator
# Write a function called generate_password that selects three random words from a provided file of words and concatenates them into a single string. The code to read in the data from the file is already in the starter code, you will need to build a password out of these parts.
# Use an import statement at the top
import random

word_file = "words.txt"
word_list = []
random_words = []
# fill up the word_list
with open(word_file, 'r') as words:
    for line in words:
        # remove white space and make everything lowercase
        word = line.strip().lower()
        # don't include words that are too long or too short
        if 3 < len(word) < 8:
            word_list.append(word)


# Add your function here
# It should return a string consisting of three random words
# concatenated together without spaces
def generate_password():
    random_words = random.sample(word_list, k=3)
    return ''.join(random_words)


def generate_password():
    return random.choice(word_list) + random.choice(word_list) + random.choice(word_list)


print(generate_password())


# Quiz: The continue_crawl Function
# The first helper function we need to write is continue_crawl which will be used in our while loop like this:
#
# while continue_crawl(search_history, target_url):
# For example, we might call the function with these values:
#
# continue_crawl(['https://en.wikipedia.org/wiki/Floating_point'],
#                        'https://en.wikipedia.org/wiki/Philosophy')
# search_history is a list of strings which are the urls of Wikipedia articles. The last item in the list most recently found url.
# target_url is a string, the url of the article that the search should stop at if it is found.
# continue_crawlshould return True or False following these rules:
#
# if the most recent article in the search_history is the target article the search should stop and the function should return False
# If the list is more than 25 urls long, the function should return False
# If the list has a cycle in it, the function should return False
# otherwise the search should continue and the function should return True.
# For this quiz, implement continue_crawl. For each of the situations where the search stops, print a message that briefly explains why. Remember to test your code!
def continue_crawl(search_history, target_url, max_steps=25):
    if search_history[-1] == target_url:
        print("We've found the target article!")
        return False
    elif len(search_history) > max_steps:
        print("The search has gone on suspiciously long, aborting search!")
        return False
    elif search_history[-1] in search_history[:-1]:
        print("We've arrived at an article we've already seen, aborting search!")
        return False
    else:
        return True


print(continue_crawl(['https://en.wikipedia.org/wiki/Floating_point',
                      'https://en.wikipedia.org/wiki/Computing',
                      'https://en.wikipedia.org/wiki/Floating_point'], 'https://en.wikipedia.org/wiki/Philosophy'))
